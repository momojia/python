import sys

from PyQt5.QtWidgets import QApplication, QWidget

if __name__ == "__main__":
    app = QApplication(sys.argv)
    windows = QWidget()
    windows.resize(800, 500)
    windows.move(300, 300)
    windows.setWindowTitle("PyQT5程序")
    windows.show()
    sys.exit(app.exec())